#pragma once
#include"stdafx.h"

class Network
{
public:
	Network();
	virtual ~Network();

	void Initalize();
private:
	WSADATA mWsadata;
protected:
	SOCKET mListenSocket;
};