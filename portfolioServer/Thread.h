#pragma once
#include<thread>
#include<mutex>

#define MAKE_THREAD(ClassName, method) (new Thread(new thread(&ClassName##::##method, this)))
class Thread
{
public:
	Thread(thread * inThread, wstring inName);
	~Thread();
private:
	int32 mId;
	Lock * mLock;
	thread * mThread;
	wstring mName;
	int mNumbers;
};

