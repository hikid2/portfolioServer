#include "stdafx.h"
#include "MovementComponent.h"


MovementComponent::MovementComponent()
{
}


MovementComponent::~MovementComponent()
{
}

/**
	@return : void

	@param1 : deque로 뭉쳐져 있는 Data

	@param2 : Controller가 소유중운 Actor

	@brief : Tick에 Update해야 할 데이터 정보 처리(이동 등등)

	@warning : 
*/

void MovementComponent::TickComponent(deque<KeyPair>& inData, const shared_ptr<Actor>& inObject)
{
	double velocity = inObject->GetVelocity();
	while (!inData.empty())
	{
		KeyPair key = inData.front();

		PerformMovement(key, velocity);

		// TODO : ColiedCheck(), OnMoveUpdate(inObject) 직접 구현 후 주석 해제
		//ColiedCheck();

		//OnMoveUpdate(inObject);

		inData.pop_front();
		
	}
}

/**
@return : 키 데이터를 디코더한 Normalize Direction vector 를 반환

@param 1 : 인코딩된 키 데이터

@brief : bit로 인코딩된 키 데이터를 방향 벡터로 decode하여 return 한다.

@warning : 이동값만 디코딩됨.(노멀라이징 된 vector를 반환)
*/
Vector3 MovementComponent::OutPutDirection(const int32 & IncodeKeyData)
{
	Vector3 direction = Vector3(0.f, 0.f, 0.f);
	if (IncodeKeyData & KEYDATA::LEFTWARD) direction.x -= 1;
	if (IncodeKeyData & KEYDATA::RIGHTWARD) direction.x += 1;
	if (IncodeKeyData & KEYDATA::FORWARD) direction.z += 1;
	if (IncodeKeyData & KEYDATA::BACKWARD) direction.z -= 1;
	D3DXVec3Normalize(&direction, &direction);
	return direction;
}
