#pragma once

#pragma warning(disable:4996)
const int SERVERPORT = 9230;
const int SOCK_BUFSIZE = 2048;
const int MAXPLAYER = 5000;

enum KEYDATA 
{
	FORWARD = 1 << 0, 
	BACKWARD = 1 << 1,
	LEFTWARD = 1<< 2,
	RIGHTWARD = 1 << 3,
	JUMP = 1<< 4,
	ATTACK = 1 << 5,
};

typedef Json::Value jsonValue_t;
typedef Json::Reader jsonReader_t;

#undef	SAFE_DELETE
#define SAFEDELETE(obj)						\
{												\
	if ((obj)) delete(obj);		    			\
    (obj) = 0L;									\
}
//-------------------------------------------------------------------//
//문자열 변환   멀티바이트  -> 유니코드  or 유니코드 ->  멀티바이트
inline void StrConvA2T(CHAR *src, TCHAR *dest, size_t destLen) {
#ifdef  UNICODE                     // r_winnt
	if (destLen < 1) {
		return;
	}
	MultiByteToWideChar(CP_ACP, 0, src, -1, dest, (int)destLen - 1);
#endif
}

inline void StrConvT2A(TCHAR *src, CHAR *dest, size_t destLen) {
#ifdef  UNICODE                     // r_winnt
	if (destLen < 1) {
		return;
	}
	WideCharToMultiByte(CP_ACP, 0, src, -1, dest, (int)destLen, NULL, FALSE);
#endif
}

inline void StrConvA2W(CHAR *src, WCHAR *dest, size_t destLen) {
	if (destLen < 1) {
		return;
	}
	MultiByteToWideChar(CP_ACP, 0, src, -1, dest, (int)destLen - 1);
}
inline void StrConvW2A(WCHAR *src, CHAR *dest, size_t destLen) {
	if (destLen < 1) {
		return;
	}
	WideCharToMultiByte(CP_ACP, 0, src, -1, dest, (int)destLen, NULL, FALSE);
}
//-------------------------------------------------------------------//

