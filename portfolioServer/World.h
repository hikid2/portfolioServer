#pragma once
#include<set>
#include<algorithm>

/*
*	Zone 최소 2개 구성
*	512 * 512의 하이트맵
*	그리드는 10으로 주면
*	5120 * 5120
*	20개로 나누고 싶다.
*	1개의 영역은 256 * 256
*	시야거리 20 으로 구성
*	
*/
typedef int Hsize;
typedef int Vsize;
typedef pair<Hsize, Vsize> Sizes;

//class QNode
//{
//public:
//	QNode() {}
//	~QNode(){}
//	QNode(int inNowlevel, int inMaxLevel, int inHorizeon, int inVertical) 
//	{
//		/*
//			129 129 
//		*/
//		mNowLevel = inNowlevel;
//		mHorizon = inHorizeon;
//		mVertical = inVertical;
//		Initialize(inMaxLevel);
//	}
//	QNode(int inNowlevel, int inMaxLevel, int startH, int startV, int wid, int hig)
//	{
//		/*
//		1. 시작범위 너비
//		- 시작 범위 + 너비 , 시작범위  + (너비/ 2)
//		2. 끝 범위 너비
//		*/
//		inNowlevel++;
//		auto hlfw = wid / 2;
//		auto hlfh = hig / 2;
//		for (auto itery = 0; itery < 2; ++itery)
//		{
//			for (auto iterx = 0; iterx < 2; ++iterx)
//			{
//				mChildNode1 = new QNode(inNowlevel, inMaxLevel, );
//			}
//		}
//		
//	}
//	void Initialize(int MaxLevel)
//	{
//		
//		try
//		{
//			if (MaxLevel < mNowLevel)
//			{
//				(mHorizon - 1) - (mHorizon / 2);
//				(mVertical - 1) - (mVertical / 2);
//			}
//			else
//				throw exception("MaxLevel Closer");
//		}
//		catch (const std::exception& e)
//		{
//			Log("%s", e.what());
//		}
//	}
//private:
//	QNode * mChildNode1;
//	QNode * mChildNode2;
//	QNode * mChildNode3;
//	QNode * mChildNode4;
//
//	// Vector3 RightVertax;
//	int mHorizon;
//	int	mVertical;
//
//	int mNowLevel;
//};
//class QManager
//{
//public:
//	QManager(){}
//	QManager(int inMaxLevel, int inHorizeon, int inVertical) 
//		: mMaxLevel(inMaxLevel) { Initialize(inHorizeon, inVertical); }
//	~QManager(){}
//	void Initialize(int inHorizeon, int inVertical)
//	{
//		int nowlevel = 0;
//		mNode = new QNode(nowlevel, mMaxLevel,inHorizeon, inVertical);
//	}
//private:
//	int mMaxLevel;
//	QNode * mNode;
//};
class Sector
{
public:
	Sector(){}
	Sector(int h, int v)
	{ setHV(h, v); }
	~Sector() 
	{
		for (auto iter : mGameObjectList)
		{
			iter = nullptr;
		}
	}
	void setHV(int h, int v)
	{
		 mHorizonal = h * mGrid;
		 mVertical = v * mGrid;
	}
	RECT getsize()
	{
		//Sizes ret = make_pair(mHorizonal + mGrid, mVertical + mGrid);
		RECT r;
		r.left = mHorizonal;
		r.right = mHorizonal + mGrid;
		r.top = mVertical;
		r.bottom = mVertical + mGrid;
		return r;
	}
	void print()
	{
		auto var = getsize();
		cout << "left : " << var.left << 
			"righr : " << var.right << 
			"top : " << var.top << 
			" bot : " << var.bottom << endl;
	}
	
	void InsertGameObject(GameObject * inObject)
	{	
		mGameObjectList.insert(inObject);
	}
	
	const set<GameObject *> & getGameObjectList()const 
	{
		return mGameObjectList;
	}
	void EraseGameObject(GameObject * inObject) {
		mGameObjectList.erase(inObject);
	}
	bool Find(GameObject * inObject)
	{
		try
		{
			auto iter = mGameObjectList.find(inObject);
			if (iter == mGameObjectList.end())
				throw exception();
		}
		catch (const std::exception& e)
		{
			return false;
		}
		return true;
	}
	static const int getGrid() { return mGrid; }
private:
	int mHorizonal;
	int mVertical;
	static const int mGrid = 256;
	set<GameObject * > mGameObjectList;
};


class Zone
{
public:
	Zone() : mCnt(20)
	{
		Initialize();
	}
	void Initialize()
	{
		for (int v = 0; v < mCnt; ++v)
		{
			for (int h = 0; h < mCnt; ++h)
			{
				auto var = new Sector(h, v);
				mZoneList.emplace_back(var);
			}
		}
	}
	void Print()
	{
		for (auto iter : mZoneList)
		{
			iter->print();
		}
	}
	virtual ~Zone()
	{
		for (auto & iter : mZoneList)
		{
			delete iter;
		}
	}
	void Insert(GameObject * inObject)
	{
		auto pos = inObject->getPosition();
		auto h = (int32)pos.z / Sector::getGrid();
		auto v = ((int32)pos.x / Sector::getGrid()) * 20;
		auto retval = h + v;
		mZoneList[retval]->InsertGameObject(inObject);
	}

private:
	vector<Sector *> mZoneList;
	const int mCnt;
};


class instanceDungeon
{
public:

private:

};

class World
{
public:
	World();
	~World();
private:
};

