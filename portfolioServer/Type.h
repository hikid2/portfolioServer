#pragma once
#include<cstdlib>
#include<string>
#include<mutex>

// strings..
typedef wstring wstr;
typedef string	str;
typedef size_t	PackSize;
typedef wchar_t wchar;

// int, uint
typedef int64_t int64;
typedef int32_t int32;
typedef uint64_t uint64;
typedef uint32_t uint32;

typedef D3DXVECTOR3 Vector3;

typedef recursive_mutex mtx;


typedef pair<double, int32> KeyPair;