#include "stdafx.h"
#include "Controller.h"

// Teat Main Mathod
void main()
{
	
}



void AuthorityController::Simulation()
{
	try
	{
		if (!SharedPtrValidCheck())
			throw exception("EffectivenessCheck Failed..");

		if (mInputData->Empty()) return;

		deque<KeyPair> pushData;
		mInputData->Copy(pushData);

		mMoveMentComponent->TickComponent(pushData, mGameObject);
	}
	catch (const std::exception& ex)
	{
		Log("%s", ex.what());
	}
	
}

bool AuthorityController::SharedPtrValidCheck()
{
	try
	{
		if (mGameObject.get() == nullptr)
			throw exception("Not Create GameObject ");
		if (this->mInputData.get() == nullptr)
			throw exception("Not Create mInputData ");
		if (this->mInputImpl.get() == nullptr)
			throw exception("Not Create mInputImpl ");
		if (this->mMoveMentComponent.get() == nullptr)
			throw exception("Not Create mMoveMentComponent ");
	}
	catch (const std::exception& ex)
	{
		Log("%s", ex.what());
		return false;
	}
	return true;
}

void AuthorityController::Read(ReadStream * inStream)
{

}

void AuthorityController::Write(WriteStream & inStream)
{

}





/**
@return : void

@param 1 : forward, backward key data

@param2 : right, left key data

@brief : 키입력에 대한 정보에 따라 패킷에 송신 할 데이터를 생성.

@warning : Testing..
*/
void InputImpl::InputDataencodeBit(const int32 & inLook, const int32 & inRight)
{
	SetSnap();
	Clear();
	horizontal(inRight);
	vertical(inLook);
}

/**
@return : first[double] : Tick, second[int32] : KeyData

@param 1 : void

@brief : 키 입력 시간과 키 입력의 데이터를 반환

@warning :
*/
KeyPair InputImpl::GetKeyPair() {
	KeyPair temp = make_pair(mTickCount, mInputKey);
	return temp;
}

/**
@return : void

@param 1 : inRight (1 : right, -1 : left)

@brief : 좌우측 키 입력에 따른 패킷 전송용 데이터 변환

@warning :
*/
void InputImpl::horizontal(const int32 & inRight)
{
	switch (inRight)
	{
	case 0:
		break;
	case 1:
		mInputKey += RIGHTWARD;
		break;
	case -1:
		mInputKey += LEFTWARD;
		break;
	default:
		break;
	}
}

/**
@return : void

@param 1 : inLook (1 : forward, -1 : backward)

@brief : 좌우측 키 입력에 따른 패킷 전송용 데이터 변환

@warning :
*/
void InputImpl::vertical(const int32 & inLook)
{
	switch (inLook)
	{
	case 0:
		break;
	case 1:
		mInputKey += FORWARD;
		break;
	case -1:
		mInputKey += BACKWARD;
		break;
	default:
		break;
	}
}

/**
@return : void

@param 1 : KeyPair(First : Tick, Second : Keydata)

@brief : 키입력 관련 데이터 deque에 Push

@warning : Push Backd을 사용.
*/
void InputData::push(const KeyPair & inKey)
{
	mInputData.push_back(inKey);
}

/**
@return : void

@param 1 : 복사내용을 저장할 deque<KeyPair> &

@brief : mInputData의 infomation을 inDeque 에 Copy.

@warning : Null...
*/
void InputData::Copy(deque<KeyPair>& inDeque)
{
	std::copy(mInputData.begin(), mInputData.end(), std::back_inserter(inDeque));
	//std::swap(inDeque, mInputData);
}

/**
@return : KeyPair

@brief : in mInputData FrontData Pop And return FrontData return

@warning : 테스팅이 필요.
*/
KeyPair InputData::pop()
{
	auto tempData = mInputData.front();
	mInputData.pop_front();
	return tempData;
}

/**
@return : bool(true : 비어있음 , false : 비어있지 않음)

@brief : null

@warning : null
*/
bool InputData::Empty()
{
	return mInputData.empty();
}

/**
@return : void

@param 1 : KeyPair(First : Tick, Second : Keydata)

@brief : Lock으로 감싼 버전

@warning : Push Backd을 사용.
*/
void ServerInputData::push(const KeyPair & inKey)
{
	mMutex.lock();
	InputData::push(inKey);
	mMutex.unlock();
}


/**
@return : void

@param 1 : 복사내용을 저장할 deque<KeyPair> &

@brief : mInputData의 infomation을 inDeque 에 Copy.(Lock으로 감싼 버전)

@warning : Null...
*/
void ServerInputData::Copy(deque<KeyPair>& inDeque)
{
	mMutex.lock();
	InputData::Copy(inDeque);
	mMutex.unlock();
}


/**
@return : KeyPair

@brief : in mInputData FrontData Pop And return FrontData return(Lapping to Lock)

@warning : Please to Do Testing...
*/
KeyPair ServerInputData::pop()
{
	mMutex.lock();
	auto retval = InputData::pop();
	mMutex.unlock();
	return retval;
}

/**
@return : bool(true : 비어있음 , false : 비어있지 않음)

@brief : null

@warning : null
*/
bool ServerInputData::Empty()
{
	mMutex.lock();
	auto retval = InputData::Empty();
	mMutex.unlock();
	return retval;
}



void ClientController::Simulation()
{

}

bool ClientController::SharedPtrValidCheck()
{
	return false;
}

void ClientController::Read(ReadStream * inStream)
{

}

void ClientController::Write(WriteStream & inStream)
{
	if (!mInputData->Empty())
	{
		mInputData->Write(inStream);
	}
}
