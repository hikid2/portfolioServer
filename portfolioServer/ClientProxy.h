#pragma once
#include<array>
/*
** 클라 -> 서버 로그인 패킷(id pw)
** 클라 -> 서버 회원가입 패킷(id pw name birth day)
** 클라 -> 서버 캐릭터 생성(캐릭터 종류, 닉네임)
** 클라 -> 서버 인게임 입장()
** 클라 -> 서버 인풋 데이터(키 입력 데이터)
** 클라 -> 서버 게임 나가기()
** 클라 -> 서버 아이템 구매()

** 서버 -> 클라 로그인 관련 패킷
** 서버 -> 클라 회원가입 관련 패킷
** 서버 -> 클라 생성된 캐릭터 정보 전달  
** 서버 -> 클라 인게임 정보를 전달
** 서버 -> 클라 주변 유저중 렌더링에 필요한 유저 정보를 전달
** 서버 -> 클라 캐릭터 리플리케이션(좌표 정보) 정보 전달



** 서버 -> 클라 주기적 패킷은 30 fps로 고정
*/
enum PacketType : uint64
{
	Hello = 1 << 0,
	Login = 1 << 1,
	timeout = 1 << 2,
	join = 1 << 3,
	Heartbeat = 1 << 4,
	SelectCharacter = 1 << 5,
	MoveMent = 1 << 6,
	PlayerData = 1 << 7,
	Exit = 1 << 10,
};

class Contents
{
	typedef void(*ContentsFunc)(ReadStream * , Session *);

public:
	Contents() { Initialize(); }
	virtual ~Contents() {}
	void Initialize()
	{
		mContentsFuncs.insert(make_pair(PacketType::Login, &Contents::Login_Method));
		mContentsFuncs.insert(make_pair(PacketType::Hello, &Contents::Hello_Method));
		mContentsFuncs.insert(make_pair(PacketType::join, &Contents::Join_Method));
		mContentsFuncs.insert(make_pair(PacketType::SelectCharacter, &Contents::SelectCharacter_Method));
		mContentsFuncs.insert(make_pair(PacketType::Exit, &Contents::Exit_Method));
	}
	void Run(const uint64 & inData, ReadStream * inStream, Session * inSession)
	{
		try
		{
			auto retFunc = mContentsFuncs.find(inData);
			if (retFunc == mContentsFuncs.end()) 
				throw exception("Invalid InData Numbers...");
			retFunc->second(inStream, inSession);
		}
		catch (const std::exception& e)
		{
			Log("[%s errer] ", e.what());
		}
		
	}
protected:
	static void Hello_Method(ReadStream *, Session *) {}
	static void Login_Method(ReadStream *, Session *) {}
	static void Join_Method(ReadStream *, Session *) {}
	static void SelectCharacter_Method(ReadStream *, Session *) {}
	static void Exit_Method(ReadStream *, Session *) {}
private:
	unordered_map<uint64, ContentsFunc> mContentsFuncs;
};


class PacketProxy
{
public:
	void PacketAnalize(const DWORD & inRecvBufferSize)
	{
		int offset = 0;
		array<unsigned char, SOCK_BUFSIZE> buf;
		try
		{
			DWORD totalbytes = PickUpTotalSize(buf);
			offset += sizeof(DWORD);
			auto retval = CompareCurrentAndTotal(inRecvBufferSize, totalbytes);
			if (retval == -1) {}
			else if (retval == 1) {}
		}
		catch (const std::exception&)
		{

		}
	}
	void HelloProcess(ReadStream * inStream, Session * inSession)
	{
		int header = 0;
		inStream->Read(&header);
		if (PacketType::Login & header) { 
			mContent.Run(PacketType::Login, inStream, inSession); 
			return; 
		}
		if(PacketType::join & header){
			mContent.Run(PacketType::join, inStream, inSession);
			return;
		}
		if(PacketType::timeout & header){
			mContent.Run(PacketType::timeout, inStream, inSession);
			return;
		}
		if(PacketType::Exit & header){
			mContent.Run(PacketType::Exit, inStream, inSession);
			return;
		}
	}
	void SelectProcess(ReadStream * inStream, Session * inSession)
	{
		int packetHeader = 0;
		inStream->Read(&packetHeader);
	}
	void IngameProcess(ReadStream * inStream, Session * inSession)
	{
		int packetHeader = 0;
		inStream->Read(&packetHeader);
	}
private:
	DWORD PickUpTotalSize(const array<unsigned char, SOCK_BUFSIZE> & inBuffer)
	{
		DWORD totalSize = 0;
		memcpy_s(&totalSize, sizeof(totalSize), inBuffer.data(), sizeof(DWORD));
		return totalSize;
	}
	int CompareCurrentAndTotal(const DWORD & inRecvBufferSize, const DWORD & inTotalBytes)
	{
		if (inRecvBufferSize != inTotalBytes)
		{
			if (inRecvBufferSize > inTotalBytes)
			{
				return 1;
			}
			return -1;
			
		}
		return 0;
	}
	Contents mContent;
};

/**
	@class	ClientProxy
	@data	2017.10.09
	@author	saem
	@brief	Controller와 Session을 결합하여 관리
	@warning 기능적으로 무쓸모시 삭제 될 수 있음
*/
class ClientProxy
{
	enum ConnectStatus
	{
		Hello,
		Select,
		Ingame,
	};


public:
	ClientProxy();
	~ClientProxy();

	/**
		@return : void

		@param 1 : 결합할 SessionPtr

		@brief : 세션을 ClientProxy와 결합

		@warning : 해당 기능은 삭제될 가능성이 있습니다.
	*/
	void AttachSession(Session * inSessionPtr) { mSession = inSessionPtr; }

	/**
	@return : void

	@brief : 세션의 소유를 해제 합니다.

	@warning : null
	*/
	void DetachSession() { mSession = nullptr; }

	void CreateController()
	{

	}
	/**
		@return :

		@param 1 :

		@brief :

		@warning : 
	*/
	virtual void AnalizeStream(ReadStream * inStream)
	{
		switch (mConnectStatus)
		{
		case ClientProxy::Hello:
			mPacketProxy.HelloProcess(inStream, mSession);
			break;
		case ClientProxy::Select:
			mPacketProxy.SelectProcess(inStream, mSession);
			break;
		case ClientProxy::Ingame:
		{
			AuthorityController * ptr = dynamic_cast<AuthorityController*>(mController);
			ptr->Read(inStream);
			mPacketProxy.IngameProcess(inStream, mSession);
		}
			break;
		default:
			break;
		}
	}
	

	Session * GetSession() const { return mSession; }
	void Clear();
	bool IsUse() { return mIsUse; }
	void SetUse(bool inUse) { mIsUse = inUse; }
	void Update()
	{
		try
		{
			if (!mIsUse  || mSession == nullptr) throw;
			
		}
		catch (const std::exception&)
		{
			return;
		}
		mController->Simulation();
	}

	const Controller * GetController()const { return mController; }
private:
	Session * mSession;
	Controller * mController;
	bool mIsUse = false;
	PacketProxy mPacketProxy;
	ConnectStatus mConnectStatus;
};


class ClientProxyManager : public Singleton<ClientProxyManager>
{
public:
	ClientProxyManager(){
		for (auto cnt = 0; cnt < mProxyAry.max_size(); ++cnt)
		{
			mProxyAry[cnt] = new ClientProxy();
		}
	}
	virtual ~ClientProxyManager() 
	{
		Relase();
	}
	virtual void Relase()
	{
		for (auto cnt = 0; cnt < mProxyAry.max_size(); ++cnt)
		{
			if (mProxyAry[cnt] != nullptr)
				delete mProxyAry[cnt];
			mProxyAry[cnt] = nullptr;
		}
	}

	void Add(Session * inSession)
	{
		try
		{
			for (auto & iter : mProxyAry)
			{
				if (!iter->IsUse()) {
					iter->AttachSession(inSession);// inProxy
					return;
				}
			}
			throw exception("not able Add Method..");
		}
		catch (const std::exception& ex)
		{
			Log("%s", ex.what());
		}
		return;
	}

	void Erase(Session * inSession);
	ClientProxy * Find(Session * inSession);
	
	bool IsFull()
	{
		try
		{
			for (auto iter : mProxyAry)
			{
				if (iter->IsUse()) continue;
				else
					return false;
			}
			throw;
		}
		catch (const std::exception&)
		{
			return true;
		}
	}
private:
	array<ClientProxy *, MAXPLAYER> mProxyAry;
};

