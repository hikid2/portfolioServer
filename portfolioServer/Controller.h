#pragma once
#include<deque>



class InputImpl
{
public:
	InputImpl() : mTickCount(0.0f){}
	~InputImpl(){}

	void InputDataencodeBit(const int32 & inLook, const int32 & inRight);

	
	KeyPair GetKeyPair();
	
private:
	int32 mInputKey = 0;
	double mTickCount = 0.0f;

	void horizontal(const int32 & inRight);
	void vertical(const int32 & inLook);

	void Clear() { mInputKey = 0; }
	void SetSnap() { mTickCount = Timer::getInstance().GetNowTime(); }
};

class InputData
{
public:
	InputData() : mCalledLatelyTime(0.0f) {}
	virtual ~InputData(){}

	virtual void push(const KeyPair & inKey);

	virtual void Copy(deque<KeyPair> & inDeque);

	virtual KeyPair pop();

	virtual bool Empty();
	
	void Write(WriteStream & inStream)
	{
		std::deque<KeyPair> keydeq;
		this->Copy(keydeq);
		size_t cnt = keydeq.size();
		inStream.Write(cnt);
		while (!keydeq.empty())
		{
			inStream.Write(keydeq.front().first);
			inStream.Write(keydeq.front().second);
			keydeq.pop_front();
		}
		
	}
	
private:
	deque<KeyPair> mInputData;
	double mCalledLatelyTime = 0.0f;
};

class ServerInputData : public InputData
{
public:
	ServerInputData(){}
	virtual ~ServerInputData(){}

	virtual void push(const KeyPair & inKey);

	virtual void Copy(deque<KeyPair> & inDeque);

	virtual KeyPair pop();

	virtual bool Empty();
private:
	Lock mMutex;
};

class ClientInputData : public InputData
{
public:
	ClientInputData(){}
	virtual ~ClientInputData(){}
private:
	
};

/**
@class	Controller
@data	2017.09.29
@author	saem
@brief	컨트롤러의 기본 Pure Virtual Method provided
@warning 컨트롤러의 기능을 추가 하는것이 필요..
*/
class Controller
{
public:
	Controller(){}
	virtual ~Controller(){}
	virtual void Simulation() = 0;
	virtual bool SharedPtrValidCheck() = 0;
	virtual void Read(ReadStream * inStream) = 0;
	virtual void Write(WriteStream & inStream) = 0;
	
};

class AuthorityController : public Controller
{
public:
	AuthorityController() : mInputImpl(make_shared<InputImpl>()), mInputData(make_shared<ServerInputData>()) , mGameObject(nullptr){}
	virtual ~AuthorityController() { mInputImpl = nullptr; }
	virtual void Simulation() override;
	virtual bool SharedPtrValidCheck() override;
	virtual void Read(ReadStream * inStream) override;
	virtual void Write(WriteStream & inStream) override;
private:
	shared_ptr<InputImpl>	mInputImpl;
	// Key Data
	shared_ptr<InputData>	mInputData;


	shared_ptr<Component>	mMoveMentComponent;
	shared_ptr<Actor>	mGameObject;
};

class ClientController : public Controller
{
public:
	ClientController() : mInputImpl(make_shared<InputImpl>()), mInputData(make_shared<ClientInputData>()) {}
	virtual ~ClientController(){}
	virtual void Simulation() override;
	virtual bool SharedPtrValidCheck() override;
	virtual void Read(ReadStream * inStream) override;
	virtual void Write(WriteStream & inStream) override;
private:
	shared_ptr<InputImpl>	mInputImpl;
	shared_ptr<InputData>	mInputData;

	shared_ptr<Actor>	mGameObject;
	shared_ptr<Component>	mMoveMentComponent;
};
//class AIController
//{
//public:
//private:
//	AILogic * mAILogic;
//};
class AILogic
{
public:
	AILogic(){}
	virtual ~AILogic(){}
	virtual void Update() = 0;
	virtual void Run() = 0;
	virtual void Process() = 0;
};