#pragma once
#include<chrono>
using namespace std::chrono;


class Timer : public Singleton<Timer>
{
	typedef duration<double, milli> doublemillisec;
public:
	Timer()
	{
		mStartTick = chrono::system_clock::now(); 
		mUpdateTick = chrono::system_clock::now(); 
		mAverageTick = 0.0f;
		mFpsTick = chrono::system_clock::now();
		mCount = 0;
		mNowFPS = 0;
		mLastdurationTick = 0.0f;
	}
	~Timer(){}
	void Update()
	{
		++mCount;

		doublemillisec result = duration_cast<doublemillisec>(chrono::system_clock::now() - mFpsTick);
		mLastdurationTick = duration_cast<doublemillisec>(chrono::system_clock::now() - mUpdateTick).count();
		if (result.count() > 1000.0f)
		{
			mNowFPS = mCount;
			mAverageTick = (result.count() / 1000.0f);
			mCount = 0;
			mFpsTick = chrono::system_clock::now();
		}

		mUpdateTick = chrono::system_clock::now();
		
	}
	double AverageTick() { return mAverageTick; }
	int NowFPS() { return mNowFPS; }
	double GetDurationTick() { return mLastdurationTick; }
	double GetNowTime() 
	{
		return chrono::duration_cast<doublemillisec>(chrono::system_clock::now() - mStartTick).count();
	}
private:
	chrono::time_point<chrono::system_clock> mStartTick;	// 시작 point
	chrono::time_point<chrono::system_clock> mUpdateTick;	// Update 함수 call 시 point
	chrono::time_point<chrono::system_clock> mFpsTick;		// 

	double mLastdurationTick;
	int mCount;
	int mNowFPS;
	double mAverageTick;
};