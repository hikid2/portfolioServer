#pragma once
#include<map>
class ThreadManager : public Singleton<ThreadManager>
{
public:
	ThreadManager();
	virtual ~ThreadManager();

	void insert();
	void erase();
	void at();
private:
	std::map<int, Thread *> mThreadPool;
};

