// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "ws2_32.lib")

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include<iostream>

// Add Server Headers

#include<WinSock2.h>
#include<D3DX10math.h>



using namespace std;

#include"JSON\/json.h"

// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
#include"Type.h"
#include"Defines.h"

#include"Config.h"
#include"Singleton.h"

#include"Lock.h"
#include"Timer.h"
#include"Log.h"
#include"Thread.h"
#include"ThreadManager.h"


#include"Stream.h"
#include"Item.h"
#include"GameObject.h"
#include"MovementComponent.h"
#include"Controller.h"

#include"Session.h"
#include"ClientProxy.h"

#include"Network.h"
#include"IocpNetwork.h"