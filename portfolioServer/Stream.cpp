#include "stdafx.h"
#include "Stream.h"

/* Test Main
void main()
{
	WriteStream s;
	//vector<int> v;
	//list<int> v;
	deque<int>v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	s.Write(v);
	ReadStream R;
	R.set(s.GetData(), s.GetOffset());
	s.Clear();
	v.clear();
	while (!v.empty()) {
		v.pop();
	}
	R.Read(&v);
	R.Clear();
	return;
}
*/

void WriteStream::Clear()
{
	ZeroMemory(mStream.data(), mStream.size());
	mOffset = 0;
}

void WriteStream::Write(const bool & inData)
{
	memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &inData, 1);
	mOffset += 1;
}

void WriteStream::Write(const str & inData)
{
	size_t len = 0;
	len = inData.length();
	if (len == 0) return;
	Write(len);
	memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, inData.data(), len);
	mOffset += len;
}

void WriteStream::Write(const wstr & inData)
{
	size_t len = 0;
	len = inData.length();
	if (len == 0) return;
	Write(len);
	memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, inData.data(), len * sizeof(wchar));
	mOffset += len * sizeof(WCHAR);
}
