#pragma once
class IocpNetwork : protected Network
{
public:
	IocpNetwork();
	~IocpNetwork();
	void run();

	static DWORD WINAPI AcceptThread(LPVOID inServerPtr);
	static DWORD WINAPI WorkerThread(LPVOID inServerPtr);
	
	SOCKET & ListenSock();
	
	void OnAccept(SOCKET & inClinentSock, sockaddr_in & inAdd);
	HANDLE & getIocpHandle(){ return mIocpHandle; }
private:
	HANDLE mIocpHandle;
	SOCKET mListenSocket;
	SOCKADDR_IN mServerAdder;
};