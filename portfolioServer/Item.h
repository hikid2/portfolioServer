#pragma once
/*
	Item 종류
	- 소비 아이템
	- 장착 아이템(상체, 하체, 머리, 총)
	- 유지 지속성 아이템
*/
class Item
{
public:
	enum
	{
		HEAD = (100 + 1) << 1,	// 머리
		UPPERBODY = (100 + 1) << 2, // 상체
		LOWERBODY = (100 + 1) << 3, // 하체
		WEAPONBODY = (100 + 1) << 4, // 무기
		CONSUMPTION = (100 + 1) << 5, // 소비
		CONTINUEING = (100 + 1) << 6, // 지속성
	};
	Item(){}
	virtual ~Item(){}
	virtual bool isWearAble() = 0;
	virtual bool isUseAble() = 0;
	virtual bool isConsumption() = 0;
	virtual bool isWearingCheck(){}
private:
};
class HeadItem : public Item
{
public:
	HeadItem(){}
	virtual ~HeadItem(){}
private:
	int mDefensUp;
	int mFireResit;
	int mColdResit;
	int mDarkResit;
	int mHolyResit;
};
class UpperBody : public Item
{
public:
	UpperBody(){}
	virtual ~UpperBody(){}
private:
	int mDefensUp;
	int mFireResit;
	int mColdResit;
	int mDarkResit;
	int mHolyResit;
};
class LowerBody : public Item
{
public:
	LowerBody() {}
	virtual ~LowerBody(){}

private:
	int mFireResit;
	int mColdResit;
	int mDarkResit;
	int mHolyResit;
};
class WeaponBody : public Item
{
public:
	WeaponBody(){}
	virtual ~WeaponBody(){}
private:
	int mFireResit;
	int mColdResit;
	int mDarkResit;
	int mHolyResit;
};
class Comsumption : public Item
{
public:
	Comsumption(){}
	virtual ~Comsumption(){}
private:
};
class Continueing : public Item
{
public:
	
private:
	int mFireResit;
	int mColdResit;
	int mDarkResit;
	int mHolyResit;
	int mFireEff;
	int mColdEff;
	int mDarkEff;
	int mHolyEff;
};