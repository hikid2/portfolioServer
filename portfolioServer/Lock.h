#pragma once
#include<mutex>
class Lock
{
public:
	Lock();
	~Lock();
	Lock(wstr & inLockName);

	void lock();
	void unlock();
	const wchar_t * Getname();
private:
	mtx		mLock;
	wstr	mLockName;
};

