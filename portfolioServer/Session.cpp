#include "stdafx.h"
#include "Session.h"


Session::Session() : mSockData()
{
	mTotalSize = 0;
	mCurrentSize = 0;
	for (auto index = 0; index < MAX; ++index)
		mBuffer[index].fill(NULL);
}

Session::Session(SOCKET & inSock, sockaddr_in & inAddr) : mSockData(inSock, inAddr)
{
}

Session::Session(const Session & ins)
{
	mSockData = ins.mSockData;
	mTotalSize = ins.mTotalSize;
	mCurrentSize = ins.mCurrentSize;
	for (auto index = 0; index < MAX; ++index)
	{
		mBuffer[index] = ins.mBuffer[index];
	}
}


Session::~Session()
{
}

/**
	@return : void

	@param1 : char * 형태의 에러메시지 헤더부분

	@brief : Send, Recv의 에러를 체크하여 WSAEWOULDBLOCK 이 아닌 경우 Log를 출력

	@warning : 종료를 넣지 않음.(아직은!?)
*/
void Session::SockErrorCheak(char * inMsg)
{
	int error = WSAGetLastError();
	if (error != WSAEWOULDBLOCK && error != WSA_IO_PENDING && error != 0 ) {
		LPVOID msgBuf = nullptr;
		FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)msgBuf, 0, NULL);
		Log("[ %s ERROR :  %s]", inMsg, (LPCTSTR)msgBuf);
	}
}

/**
@return : void

@brief : Totalsize 와 currentSize를 0으로 변경

@warning : None
*/
void Session::SizeClear()
{
	mTotalSize = 0;
	mCurrentSize = 0;
}

/**
@return : void

@brief : 나간 세션을 Clear

@warning : None
*/
void Session::Clear()
{
	this->SizeClear();
}

/**
@return : DataResult(COMPLETE, LACK, OVERFULL)

@param 1 : WSARecv를 통해 받은 데이터 size

@brief : WSARecv를 통해 받은 데이터의 크기의 경계를 검사

@warning : 경계만 확인하므로 이후의 후처리 필요
*/
Session::DataResult Session::RecvBoundCheck(DWORD inRecvSize)
{
	memcpy_s(&mTotalSize, sizeof(PackSize), mBuffer[READ].data(), sizeof(PackSize));

	if (mTotalSize > inRecvSize)
	{
		// TODO : Recv활동을 추가하여 남은 데이터를 받아야..
		return LACK;
	}
	if (mTotalSize == inRecvSize)
	{
		// TODO : 패킷 분별하여 작업을 행함
		return COMPLETE;
	}
	if (mTotalSize < inRecvSize)
	{
		// TODO : 패킷을 분할하여 더 받을 것과 처리할것을 구분
		return OVERFULL;
	}

}

//void SessionManager::Insert(Session * inPtr)
//{
//}

//**
//	@return  : void 
//	@param1  : broad cast message
//	@brief	 : 모든 유저에게 메시지를 전달 기능
//	@warning : 미구현이 문제
//*/
//void SessionManager::NotifyMessage(const string & inMessage)
//{
//}

/**
@return : true(success), false(fail)

@param 1 : SessionPtr

@brief : Session Insert

@warning : none
*/
bool SessionManager::Insert(Session * inPtr)
{
	try
	{
		if (inPtr == nullptr) throw exception("Param inPtr is Nullptr [Insert Mothod]");

		auto iter = std::find_if(mSessions.begin(), mSessions.end(),
			[=](unordered_map<uint64, Session *>::value_type ptr) {
			auto tempPtr = dynamic_cast<IocpSession *>(ptr.second);
			return (0 == lstrcmpW(tempPtr->GetName(), dynamic_cast<IocpSession *>(inPtr)->GetName()));
		});

		if (iter == mSessions.end()) throw exception("It is already a session [Insert Mothod]");
		dynamic_cast<IocpSession *>(inPtr)->SetUid(mIndex);
		mSessions.insert(make_pair(mIndex, inPtr));
		mUids.insert(make_pair(inPtr, mIndex));

		mIndex++;
	}
	catch (const std::exception& e)
	{
		Log("%s", e.what());
		return false;
	}
	
	return true;
}

/**
@return : true(success remove), false(fail remove)

@param 1 : Session to remove from list

@brief : 세션을 key로 Map에 있는 속성을 제거한다

@warning : 테스팅 필요
*/
bool SessionManager::Remove(Session * inPtr)
{
	try
	{
		auto iter_u = mUids.find(inPtr);
		if (iter_u == mUids.end())
			throw exception("Failed to remove session(mUids)");
		auto uid = iter_u->second;
		mUids.erase(iter_u);
		auto iter_s = mSessions.find(uid);
		if (iter_s == mSessions.end())
			throw exception("Failed to remove session(mSessions)");
		mSessions.erase(iter_s);
		SAFEDELETE(inPtr);
	}
	catch (const std::exception& e)
	{
		Log("%s", e.what());
		return false;
	}
	return true;
}

/**
@return : true(Remove Success), false(Remove Fail)

@param 1 : Session uid

@brief : uid를 통해 세션을 삭제

@warning : 테스팅 필요
*/
bool SessionManager::Remove(const uint64 & inUid)
{
	try
	{
		auto iter_s = mSessions.find(inUid);
		if (iter_s == mSessions.end())
			throw exception("Failed to remove session(mSessions)");
		auto tempPtr = iter_s->second;
		mSessions.erase(iter_s);
		auto iter_u = mUids.find(tempPtr);
		if (iter_u == mUids.end())
			throw exception("Failed to remove session(mUids)");
		mUids.erase(iter_u);
		SAFEDELETE(tempPtr);
	}
	catch (const std::exception& e)
	{
		Log("%s", e.what());
		return false;
	}
	return true;
}

/**
@return : SessionPtr (Fail 시 Nullptr )

@param 1 : SessionUid

@brief : SessionUid를 통해 SessionPtr를 반환

@warning : return 시 Nullptr 확인 필요
*/
Session * SessionManager::Get(const uint64 & inUid)
{
	try
	{
		auto iter_s = mSessions.find(inUid);
		if (iter_s == mSessions.end())
			throw exception("Not Found Session [Get Mothod]");
		return iter_s->second;
	}
	catch (const std::exception& e)
	{
		Log("%s", e.what());
		return nullptr;
	}
}

/**
@return : void

@param 1 : Message to notify

@brief : 권위자의 전체 공지사항 전달

@warning : Testing 필요
*/

inline void SessionManager::NotifyMessage(const str & inMessage)
{
	for (auto iter : mSessions)
	{
		//iter.second->SendPacket(inMessage);
	}
}

IocpSession::IocpSession() : Session()
{

}

void IocpSession::OnRecv(DWORD & inRecvSize)
{
	int offset = 0;
	memcpy_s(&mTotalSize, sizeof(PackSize), mBuffer[READ].data(), sizeof(PackSize));
	offset += sizeof(PackSize);
	try
	{
		IsRecving(inRecvSize);
		if ((mTotalSize != inRecvSize) ? throw exception("[Packet lack]") : true)
		{
			ReadStream StreamBuf;
			StreamBuf.set(mBuffer[READ].data() + offset, mTotalSize - offset);
			auto ptr = ClientProxyManager::getInstance().Find(this);
			if (ptr != nullptr)
			{
				ptr->AnalizeStream(&StreamBuf);
			}
		}
	}
	catch (const std::exception& e)
	{
		if (mTotalSize > inRecvSize)
		{
			auto wsabuf = WSABuffer(mBuffer[READ].data() + inRecvSize, mTotalSize - inRecvSize);
			this->Recv(wsabuf);
		}
		else if (mTotalSize < inRecvSize)
		{

		}
	}
}
