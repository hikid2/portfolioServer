#include"stdafx.h"
#include"Network.h"


extern bool isShutdown;
Network::Network()
{
	this->Initalize();
}

Network::~Network()
{
	WSACleanup();
	Log("WSACleanup() called");
}

void Network::Initalize()
{
	if (WSAStartup(MAKEWORD(2, 2), &mWsadata) != 0)
	{
		Log("WSAStartup Method Error..\n");
		isShutdown = true;
	}

}

//#include <string>
//int main()
//{
//	Network net;
//	
//	SOCKET socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
//	
//	sockaddr_in adder;
//	adder.sin_family = AF_INET;
//	int port = 9000;
//	adder.sin_port = htons(port);
//	adder.sin_addr.s_addr = htonl(INADDR_ANY);
//	int retval = ::bind(socket, (sockaddr*)&adder, sizeof(sockaddr_in));
//	int listenList = 5;
//	::listen(socket, listenList);
//	sockaddr_in clientAdder;
//	SOCKET clientSock;
//	int sockaddersize = sizeof(sockaddr_in);
//	while (1)
//	{
//		clientSock = accept(socket, (sockaddr*)&clientAdder, &sockaddersize);
//		char buf[1024] = {};
//		wstring str = L"hello world";
//		memcpy(buf, str.data(), (str.size() * sizeof(wchar_t)) + 1);
//		int ret = ::send(clientSock, buf, sizeof(char) * 1024, 0);
//		if (ret == -1)
//		{
//			ret = WSAGetLastError();
//		}
//		//recv(clientSock, buf, sizeof(buf) * 1024, 0);
//		printf("buf : [%ws]\n", buf);
//	}
//	WSACleanup();
//}