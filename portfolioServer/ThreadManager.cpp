#include "stdafx.h"
#include "ThreadManager.h"


ThreadManager::ThreadManager()
{
}


ThreadManager::~ThreadManager()
{
	for (auto iter = mThreadPool.begin(); iter != mThreadPool.end(); ++iter)
	{
		SAFEDELETE(iter->second);
	}
	mThreadPool.clear();
}
