#pragma once

class Map;

class Steat
{
public:
	Steat() : mMaxHp(0), mCurrentHp(0), mMaxStrikingPowrer(0),
		mMinStrikingPowrer(0), mDefence(0),
		mVelocity(0), mLastLevelPoint(0)
	{}
	virtual ~Steat() {}

	void Initalize(
		int32 inMaxhp, int32 inCurHp, int32 inMaxStrikingPowrer,
		int32 inMinStrikingPowrer, int32 inDefence, int32 inVelocity,
		int32 inFireRegi, int32 inColdRegi, int32 inDarkRegi, int32 inHolyRegi,
		int32 inFireEff, int32 inColdEff, int32 inDarkEff, int32 inHolyEff, Map * inLastLevelPoint)
	{
		mMaxHp = inMaxhp;
		mCurrentHp = inCurHp;

		mMaxStrikingPowrer = inMaxStrikingPowrer;
		mMinStrikingPowrer = inMinStrikingPowrer;

		mDefence = inDefence;

		mVelocity = inVelocity;
		mLastLevelPoint = inLastLevelPoint;

		mFireRegi = inFireRegi;
		mColdRegi = inColdRegi;
		mDarkRegi = inDarkRegi;
		mHolyRegi = inHolyRegi;

		mFireEff = inFireEff;
		mColdEff = inColdEff;
		mDarkEff = inDarkEff;
		mHolyEff = inHolyEff;
	}

	int32 GetCurrentHp()const { return mCurrentHp; }
	int32 GetMaxStrkP()const { return mMaxStrikingPowrer; }
	int32 GetMinStrkP()const { return mMinStrikingPowrer; }
	int32 GetDefence()const { return mDefence; }
	const Map * GetLastLevelPoint() { return mLastLevelPoint; }


	void SetCurrentHp(const int32 & inCurrenthp) { mCurrentHp = inCurrenthp; }
	void SetMaxStrkP(const int32 & inMaxStrkP) { mMaxStrikingPowrer = inMaxStrkP; }
	void SetMinStrkP(const int32 & inMinStrP) { mMinStrikingPowrer = inMinStrP; }
	void SetDefence(const int32 & inDefence) { mDefence = inDefence; }
	void SetLastLevelPoint(Map * const inLevelPosition) { mLastLevelPoint = inLevelPosition; }

private:
	int32	mMaxHp;
	int32	mCurrentHp;

	int32	mMaxStrikingPowrer;
	int32	mMinStrikingPowrer;

	int32	mFireEff = 0;
	int32	mColdEff = 0;
	int32	mDarkEff = 0;
	int32	mHolyEff = 0;

	int32	mFireRegi = 0;
	int32	mColdRegi = 0;
	int32	mDarkRegi = 0;
	int32	mHolyRegi = 0;

	int32	mDefence = 0;

	int32	mVelocity;
	Map *	mLastLevelPoint;
};

class Inventory
{
public:

private:

};
class WearingInven
{
public:

private:

};

class TestSteat
{
public:
	TestSteat(){}
	TestSteat(const double inVelocity, const int32 inHp, const int32 inLevel) 
		: mVelocity(inVelocity), mHp(inHp), mLevel(inLevel) {}
	TestSteat(const TestSteat & ins)
	{
		mVelocity = ins.mVelocity;
		mHp = ins.mHp;
		mLevel = ins.mLevel;
	}
	virtual ~TestSteat() {}

	virtual const double GetVelocity() const { return mVelocity; }
	virtual const int32 GetHp() const { return mHp; }
	virtual const int32 GetLevel() const { return mLevel; }
	virtual const int32 GetAccel() const { return mAccel; }
	virtual const int32 GetDeAccel() const { return mDeAccel; }
private:
	double mVelocity = 0.0f;
	float mAccel = 0.0f;
	float mDeAccel = 0.0f;
	int32 mHp = 0;
	int32 mLevel = 1;
};
class GameObject
{
public:
	GameObject() : mLookVector(Vector3(0.f, 0.f, 1.0f)), mRighrVector(Vector3(1.f, 0.f, 0.f)), mPosition(Vector3(0.f, 0.f, 0.f)){}
	GameObject(const Vector3 & inLookVector,const Vector3 & inRIghtVector,const Vector3 & inPosition) : 
	mLookVector(inLookVector), mRighrVector(inRIghtVector), mPosition(inPosition) {}
	GameObject(const GameObject & ins)
	{
		mLookVector = ins.mLookVector;
		mRighrVector = ins.mRighrVector;
		mPosition = ins.mPosition;
	}
	virtual ~GameObject() {}
	virtual const uint64 & ObjectId() = 0;
	const Vector3 & getPosition() { return mPosition; }
private:
	Vector3 mLookVector;
	Vector3 mRighrVector;
	Vector3 mPosition;
};

class Actor : public GameObject, public TestSteat
{
public:
	Actor() : GameObject(), TestSteat() {}
	Actor(const Vector3 &inLookVector, const Vector3 & inRIghtVector, const Vector3 & inPosition, 
		const double & inVelocity, const int32 & inHp, const int32 & inLevel)
		: GameObject(inLookVector, inRIghtVector, inPosition), TestSteat(inVelocity, inHp, inLevel)
	{}
	Actor(const Actor & ins) : GameObject(ins), TestSteat(ins) { }
	virtual ~Actor() {}
	virtual const uint64 & ObjectId() { return 'Act'; }
	virtual void Read() {}
	virtual void Write() {}
	virtual bool IsUpdate()
	{

	}
};

class Warrior : public Actor
{
public:
	Warrior() {}
	virtual ~Warrior() {}
	Warrior(const Warrior & ins) : Actor(ins) {}
	virtual const uint64 & ObejctId() { return 'War'; }
	virtual void Read()
	{

	}
	virtual void Write()
	{
	
	}
};