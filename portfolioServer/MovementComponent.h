#pragma once
#include<array>
#include<deque>
class Component
{
public:
	/*typedef enum MOVE_STATE
	{
		MOVE_WALK,
		MOVE_FALLING,
	} MOVESTATE;*/
public:
	Component() {}
	virtual ~Component() {}
	virtual void CalculateMove() = 0;
	virtual void TickComponent(deque<KeyPair> & inData, const shared_ptr<Actor> & inObject) = 0;
	//const float &  GetGravityAccle() const { return mGravityAccle; }
private:
	//const float mGravityAccle = 9.8f;
};


class MovementComponent : public Component
{
public:
	MovementComponent();
	virtual ~MovementComponent();

	// �̵� ����
	
	
	virtual void TickComponent(deque<KeyPair> & inData, const shared_ptr<Actor> & inObject);

	void PerformMovement(const KeyPair & inKeypair, const double & inVelocity)
	{
		Vector3 outDirection = OutPutDirection(inKeypair.second);
		if (!IsZero(outDirection))
		{
			auto length = inVelocity * inKeypair.first;
			mPerDirction = outDirection * length;
		}
	}


private:
	Vector3 OutPutDirection(const int32 & IncodeKeyData);
	bool IsZero(const Vector3 & inDirction) { return (inDirction.x == 0 && inDirction.y == 0 && inDirction.z == 0); }
	//MOVESTATE mState;

	Vector3 mPerDirction;
};

