#pragma once
#include<cassert>
#include<queue>
#include<list>
#include<deque>
class ReadStream
{
public:
	ReadStream() { Clear(); }
	~ReadStream() {}
	void Clear()
	{
		mOffset = 0;
		mReadPtr = 0;
		ZeroMemory(mStream.data(), mStream.size());
	}
	bool ReadBoundCheck(size_t inSize)
	{
		if (mReadPtr + inSize > mOffset)
		{
			Log(L"ERROR : Stream buffer Read Ptr is out of ranged");
			return false;
		}
		return true;
	}
	template<typename T>
	void Read(T * inData)
	{
		int len = sizeof(T);
		if (!ReadBoundCheck(len)) 
			return;
		Read((void *)inData, len);
	}
	void Read(void * inData, const size_t & inLength)
	{
		memcpy_s(inData, inLength, (void *)(mStream.data() + mReadPtr), inLength);
		mReadPtr += inLength;
	}
	void set(char * data, size_t size)
	{
		mOffset = size;
		memcpy_s(this->mStream.data(), mStream.size(), (void *)data, size);
	}
	void Read(bool * inData)
	{
		if (!ReadBoundCheck(1))
			return;
		Read((void *)inData, 1);
	}

	void Read(str * inData)
	{
		size_t len = 0;
		Read(&len);
		if (ReadBoundCheck(len) && len > 0) {
			char * buf = new char[len + 1];
			Read((void *)buf, len * sizeof(CHAR));
			buf[len] = '\0';
			inData->clear();
			*inData = buf;
			delete[] buf;
		}
	}
	void Read(wstr * inData)
	{
		size_t len = 0;
		Read(&len);
		if (ReadBoundCheck(len) && len > 0) {
			wchar * buf = new wchar[len + 1];
			Read((void *)buf, len * sizeof(wchar));
			inData->clear();
			buf[len] = '\0';
			*inData = buf;
			delete[] buf;
		}
	}
	template<typename T>
	void Read(std::vector<T> * inData)
	{
		static_assert(is_default_constructible<T>::value || is_enum<T>::value, "not Generic Read..");
		size_t len = 0;
		Read(&len);
		inData->clear();
		for (auto index = 0; index < len; ++index)
		{
			T data;
			Read(&data);
			inData->push_back(data);
		}
	}
	template<typename T>
	void Read(std::queue<T> * inData)
	{
		static_assert(is_default_constructible<T>::value || is_enum<T>::value, "not Generic Write..");
		size_t len = 0;
		Read(&len);
		size_t sizebuf = sizeof(T);
		while (len > 0)
		{
			if(ReadBoundCheck(sizebuf)) {
				T var;
				Read((void *)&var, sizebuf);
				inData->push(var);
				len -= 1;
			}
		}
	}
	template<typename T>
	void Read(std::list<T> * inData)
	{
		static_assert(is_default_constructible<T>::value || is_enum<T>::value, "not Generic Write..");
		size_t len = 0;
		Read(&len);
		size_t sizebuf = sizeof(T);
		while (len > 0)
		{
			if (ReadBoundCheck(sizebuf)) {
				T var;
				Read((void *)&var, sizebuf);
				inData->push_back(var);
				len -= 1;
			}
		}
	}
	template<typename T>
	void Read(std::deque<T> * inData)
	{
		static_assert(is_default_constructible<T>::value || is_enum<T>::value, "not Generic Write..");
		size_t len = 0;
		Read(&len);
		size_t sizebuf = sizeof(T);
		while (len > 0)
		{
			if (ReadBoundCheck(sizebuf)) {
				T var;
				Read((void *)&var, sizebuf);
				inData->push_back(var);
				len -= 1;
			}
		}
	}
	void Read(std::vector<bool> * inData)
	{
		size_t len = 0;
		Read(&len);
		for (auto index = 0; index < len; ++index)
		{
			if (ReadBoundCheck(1)) {
				bool data;
				Read(&data, 1);
				inData->push_back(data);
			}
		}
	}
	void Read(std::queue<bool> * inData)
	{
		size_t len = 0;
		Read(&len);
		while (len > 0)
		{
			if (ReadBoundCheck(1)) {
				bool var = false;
				Read((void *)&var, 1);
				inData->push(var);
				len -= 1;
			}
		}

	}
	void Read(std::list<bool> * inData)
	{
		size_t len = 0;
		Read(&len);
		while (len > 0)
		{
			if (ReadBoundCheck(1)) {
				bool var = false;
				Read((void *)&var, 1);
				inData->push_back(var);
				len -= 1;
			}
		}
	}
	void Read(std::deque<bool> * inData)
	{
		size_t len = 0;
		Read(&len);
		while (len > 0)
		{
			if (ReadBoundCheck(1)) {
				bool var = false;
				Read((void *)&var, 1);
				inData->push_back(var);
				len -= 1;
			}
		}
	}


private:
	array<char, SOCK_BUFSIZE> mStream;
	size_t mOffset;
	size_t mReadPtr;
};

class WriteStream
{
public:
	WriteStream()
	{
		this->Clear();
	}
	~WriteStream() {}
	void Clear();
// -------------------------------------------------------- //

	template<typename T>
	void Write(const T & inData);
	template<typename T>
	void Write(const std::vector<T> & inData)
	{
		static_assert(is_default_constructible<T>::value || is_enum<T>::value, "not Generic Write..");
		size_t len = 0;
		len = inData.size();
		Write(len);
		for (const auto & data : inData)
		{
			memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &data, sizeof(data));
			mOffset += sizeof(data);
		}
	}
	template<typename T>
	void Write(const std::queue<T> & inData)
	{
		static_assert(is_default_constructible<T>::value || is_enum<T>::value, "not Generic Write..");
		size_t len = 0;
		len = inData.size();
		Write(len);
		std::queue<T> temp(inData);
		while (temp.size() > 0)
		{
			T var = temp.front();
			memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &var, sizeof(var));
			mOffset += sizeof(var);
			temp.pop();
		}
	}
	template<typename T>
	void Write(const std::list<T> & inData)
	{
		static_assert(is_default_constructible<T>::value || is_enum<T>::value, "not Generic Write..");
		size_t len = 0;
		len = inData.size();
		Write(len);
		for (const auto & data : inData)
		{
			memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &data, sizeof(data));
			mOffset += sizeof(data);
		}
	}
	template<typename T>
	void Write(const std::deque<T> & inData)
	{
		static_assert(is_default_constructible<T>::value || is_enum<T>::value, "not Generic Write..");
		size_t len = 0;
		len = inData.size();
		Write(len);
		for (const auto & data : inData)
		{
			memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &data, sizeof(data));
			mOffset += sizeof(data);
		}
	}

// -------------------------------------------------------- //
	void Write(const bool & inData);
	void Write(const str & inData);
	void Write(const wstr & inData);

	void Write(const std::vector<bool> & inData)
	{
		size_t len = 0;
		len = inData.size();
		Write(len);
		for (const auto & data : inData)
		{
			memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &data, 1);
			mOffset += 1;

		}
	}
	void Write(const std::queue<bool> & inData)
	{
		size_t len = 0;
		len = inData.size();
		Write(len);
		queue<bool> temp(inData);
		while (!temp.empty())
		{
			bool var = temp.front();
			memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &var, 1);
			mOffset += 1;
			temp.pop();
		}

	}
	void Write(const std::list<bool> & inData)
	{
		size_t len = 0;
		len = inData.size();
		Write(len);
		for (const auto & data : inData)
		{
			memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &data, 1);
			mOffset += 1;

		}
	}
	void Write(const std::deque<bool> & inData)
	{
		size_t len = 0;
		len = inData.size();
		Write(len);
		for (const auto & data : inData)
		{
			memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &data, 1);
			mOffset += 1;
		}
	}
// -------------------------------------------------------- //

	UCHAR * GetData()
	{
		return mStream.data();
	}
	const size_t & GetOffset()const {
		return mOffset;
	}

private:
	array<unsigned char, SOCK_BUFSIZE> mStream;
	size_t mOffset;
};

template<typename T>
void WriteStream::Write(const T & inData)
{
	static_assert(is_default_constructible<T>::value || is_enum<T>::value, "not Generic Write..");
	memcpy_s(mStream.data() + mOffset, mStream.size() - mOffset, &inData, sizeof(inData));
	mOffset += sizeof(inData);
}
