#include "stdafx.h"
#include "Lock.h"


Lock::Lock()
{
}


Lock::~Lock()
{
}

Lock::Lock(wstr & inLockName)
{
	mLockName = inLockName;
	Log(" %s ",mLockName.data())
}

void Lock::lock()
{
	mLock.lock();
}

void Lock::unlock()
{
	mLock.unlock();
}

const wchar_t * Lock::Getname()
{
	return mLockName.data();
}
