#pragma once
#include<unordered_map>
#include<string>

enum
{
	READ, WRITE, MAX,
};
enum IO_OPERATION
{
	IO_SEND, IO_RECV, IO_END, IO_NONE
};

/**
@class		SockData
@data		recent 2017.10.9
@author		seam
@brief		클라이언트의 주소정보와 소켓을 소유
@warning	미완성이 문제
*/
class SockData
{
	OVERLAPPED		mOverlapped;
	SOCKET			mClientSock;
	sockaddr_in		mSockAdder;
	IO_OPERATION	mType;
public:
	
	SockData()
	{
		mClientSock = INVALID_SOCKET;
		ZeroMemory(&mSockAdder, sizeof(mSockAdder));
		ZeroMemory(&mOverlapped, sizeof(mOverlapped));
		mType = IO_NONE;
	}
	SockData(SOCKET & inSock, sockaddr_in & inaddr)
	{
		mClientSock = inSock;
		mSockAdder = inaddr;
		memset(&mOverlapped, 0, sizeof(mOverlapped));
		mType = IO_NONE;
	}
	~SockData(){}


	char * GetIp(){ return inet_ntoa(mSockAdder.sin_addr); }
	SOCKET & GetSocket() { return mClientSock; }
	
	OVERLAPPED * GetOverlapped() { return &mOverlapped; }
	IO_OPERATION GetType() { return mType; }
	void SetType(IO_OPERATION inType) { mType = inType; }

	SockData & operator=(const SockData & rnt)
	{
		mOverlapped = rnt.mOverlapped;
		mClientSock = rnt.mClientSock;
		mSockAdder = rnt.mSockAdder;
		return *this;
	}
};



/**
	@class		Session
	@data		recent2017.09.27
	@author		seam
	@brief		기본 세션클래스
	@warning	미완성이 문제
*/
class Session
{
public:
	typedef enum DataResult
	{
		COMPLETE, LACK, OVERFULL
	};
	typedef array<char, SOCK_BUFSIZE> BUFARRY;
	typedef array<BUFARRY, MAX>	      IOBUF;

	Session();
	Session(SOCKET & inSock, sockaddr_in & inAddr);
	Session(const Session & ins);
	
	virtual ~Session();
	virtual int SendPacket() = 0;
	virtual void RecvPacket() = 0;
	virtual void OnRecv(DWORD & inRecvSize) = 0;
	virtual void OnSend() = 0;
	virtual char * Getip() { return mSockData.GetIp(); }
	SOCKET & GetSocket() { return mSockData.GetSocket(); }
	void SockErrorCheak(char * inMsg);

	void SizeClear();
	virtual void Clear();
protected:
	PackSize	mTotalSize = 0;		// 패킷 총 사이즈
	size_t		mCurrentSize = 0;	// 현재 사이즈
	SockData	mSockData;
	IOBUF		mBuffer;

protected:
	DataResult RecvBoundCheck(DWORD inRecvSize);
};

class ClientProxy;
class ClientProxyManager;

class IocpSession : public Session
{
public:
	IocpSession();
	IocpSession(SOCKET & inSock, sockaddr_in & inAddr) : Session(inSock, inAddr) {}
	IocpSession(const IocpSession & ins) : Session(ins)
	{}
	virtual ~IocpSession(){}
	virtual int SendPacket()
	{ 
		WSABUF wsabuf;
		wsabuf.buf = mBuffer[WRITE].data();
		wsabuf.len = mBuffer[WRITE].size();// 수정 필요. 보내려고 하는크기만 

		DWORD sendSize = 0;
		DWORD flag = 0;

		DWORD retval = WSASend(
			mSockData.GetSocket(),
			&wsabuf, 
			SOCK_BUFSIZE, 
			&sendSize,
			flag,
			(LPWSAOVERLAPPED)mSockData.GetOverlapped(),
			0);
		if (retval == SOCKET_ERROR)
			SockErrorCheak("Send");
		return 0;
	}
	virtual void RecvPacket()
	{
		DWORD recvSize = 0;
		WSABUF wsabuf;
		wsabuf.buf = mBuffer[READ].data();
		wsabuf.len = (ULONG)mBuffer[READ].max_size();
		DWORD len = 1;
		DWORD flag = 0;
		

		int32 retval = WSARecv(mSockData.GetSocket(), &wsabuf, len, &recvSize, &flag,  (LPWSAOVERLAPPED)mSockData.GetOverlapped(), 0);
		if(retval == SOCKET_ERROR) 
			SockErrorCheak("Recv");
		auto result = RecvBoundCheck(recvSize);
		
	}
	
	virtual void OnRecv(DWORD & inRecvSize);
	virtual void OnSend()
	{

	}
	WSABUF WSABuffer(char * inBuf, int inLen)
	{
		WSABUF wsabuf;
		wsabuf.buf = inBuf;
		wsabuf.len = inLen;
		return wsabuf;
	}

	const wchar_t * GetName() { return mName.c_str(); }
	const uint64 & GetUid() { return mUid; }
	void * SetName(const wstr & inName) { mName = inName; }
	void SetUid(const uint64 & inUid) { mUid = inUid; }
private:
	wstr		mName = L"";
	uint64		mUid = 0;

	bool IsRecving(DWORD & inRecvSize)
	{
		auto result = RecvBoundCheck(inRecvSize);
		if (result == LACK)
		{

		}
		else if (result == OVERFULL)
		{

		}
		else
		{
			
		}
		return true;
	}
	void Recv(WSABUF inWsabuf)
	{
		DWORD recvSize = 0;
		DWORD len = 1;
		DWORD flag = 0;
		int32 retval = WSARecv(mSockData.GetSocket(), &inWsabuf, len, &recvSize, &flag, (LPWSAOVERLAPPED)mSockData.GetOverlapped(), 0);
		if (retval == SOCKET_ERROR)
			SockErrorCheak("Recv");
	}
};

/**
	@class	SessionManager
	@data	09.27
	@author	saem
	@brief	세션관리
	@warning 틀만 갖춰진 상태
*/
class SessionManager : public Singleton<SessionManager>
{
public:
	SessionManager(){ mIndex = 1; }
	virtual ~SessionManager() {}

	bool Insert(Session * inPtr);

	bool Remove(Session * inPtr);
	bool Remove(const uint64 & inUid);

	Session * Get(const uint64 & inUid);
	/**
		@return : bool( true : 같은 주소 존재, false : 같은 주소 없음 )

		@param 1 : inAddr(주소 정보)

		@brief : 주소 정보를 바탕으로 Session에 이미 같은 주소로 연결 된 소켓정보가 있는가 확인.

		@warning : 
	*/
	bool FindToAdd(const sockaddr_in inAdd)
	{
		string ip = inet_ntoa(inAdd.sin_addr);
		try
		{
			for (auto iter : mSessions) 
			{
				if (0 == strcmp(ip.data(), iter.second->Getip())) { throw exception(); }
			}
		}
		catch (const std::exception& exp)
		{
			return true;
		}
		return false;
	}

	void NotifyMessage(const str & inMessage);


private:
	// Associative uid
	unordered_map<uint64, Session *> mSessions;

	// Associative Session *
	unordered_map<Session *, uint64> mUids;
	uint64							 mIndex;
	Lock *							 mutex;
};
