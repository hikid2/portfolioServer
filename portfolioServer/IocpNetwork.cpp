#include "stdafx.h"
#include "IocpNetwork.h"

extern bool isShutdown;

IocpNetwork::IocpNetwork()
{
	mListenSocket = INVALID_SOCKET;
	mIocpHandle = nullptr;
	ZeroMemory(&mServerAdder, sizeof(mServerAdder));
}


IocpNetwork::~IocpNetwork()
{
}

void IocpNetwork::run()
{
	int numberofThreads = 8;
	// 1
	mIocpHandle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, numberofThreads);

	// 2
	mListenSocket = WSASocket(AF_INET, SOCK_STREAM, NULL, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (mListenSocket = INVALID_SOCKET)
	{
		Log(L"WSASocket Method Failed...");
		isShutdown = true;
		return;
	}
	
	mServerAdder.sin_family = AF_INET;
	mServerAdder.sin_addr.s_addr = htonl(INADDR_ANY);
	mServerAdder.sin_port = htons(SERVERPORT);

	// bind 재사용.
	// 3
	int bf = 1;
	setsockopt(mListenSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&bf, (int)sizeof(bf));

	// 4
	if (::bind(mListenSocket, (const sockaddr*)&mServerAdder, sizeof(mServerAdder)) == SOCKET_ERROR)
	{
		Log(L"Bing Method Failed...");
		isShutdown = true;
		return;
	}
	int backLog = 5;

	// 5
	if (::listen(mListenSocket, backLog) == SOCKET_ERROR)
	{
		Log(L"Listen Method Failed...");
		isShutdown = true;
		return;
	}
}


DWORD WINAPI IocpNetwork::AcceptThread(LPVOID inServerPtr)
{
	IocpNetwork * ownServer = (IocpNetwork*)inServerPtr;
	
	while (isShutdown == false)
	{
		SOCKET AcceptSock = INVALID_SOCKET;
		
		sockaddr_in ClientAdder;
		static int adderLen = sizeof(ClientAdder);

		AcceptSock = WSAAccept(ownServer->ListenSock(), (sockaddr*)&ClientAdder, &adderLen, NULL, 0);
		if (AcceptSock == INVALID_SOCKET)
		{
			Log(L"!!	Accept is Failed.....	!!");
			break;
		}
		ownServer->OnAccept(AcceptSock, ClientAdder);
	}
	return 0;
}
DWORD WINAPI IocpNetwork::WorkerThread(LPVOID inServerPtr)
{
	IocpNetwork * iocp = (IocpNetwork *)inServerPtr;
	while (!isShutdown)
	{
		DWORD getBytes = 0;
		IocpSession * session = nullptr;
		SockData * data = nullptr;

		bool retval = GetQueuedCompletionStatus(iocp->getIocpHandle(), &getBytes, (PULONG_PTR)session, (LPOVERLAPPED*)&data, INFINITE);
		//////////////////////////////////////////////////////////////////////
		// keepAlive 에서 소켓처리를 넘기는 방안 채택						//
		// Note : retval = false , transfersize = 0  : 비 정상적 종료		//
		//		  retval = true , transfersize = 0 : 정상적 종료			//
		//		  *Overlapped = null, retval = false, completionkey = null	//
		//		  : WAIT_TIMEOUT											//
		//////////////////////////////////////////////////////////////////////
		if (!retval)
		{
			if (getBytes == 0)
			{
				// 비정상 종료
				// TODO : 비정상 종료에 대한 후처리가 필요.
				ClientProxyManager::getInstance().Erase(session);
				SessionManager::getInstance().Remove(session);
			}
			continue;
		}
		auto ioType = data->GetType();
		switch (ioType)
		{
		case IO_SEND:
			session->OnSend();
			break;
		case IO_RECV:
			int64 size;
			session->OnRecv(getBytes);
			break;
		case IO_END:
			break;
		case IO_NONE:
			break;
		default:
			break;
		}
	}
	
	return 0;
}

SOCKET & IocpNetwork::ListenSock()
{
	
	return mListenSocket;
}

void IocpNetwork::OnAccept(SOCKET & inClinentSock, sockaddr_in & inAdd)
{
	// 같은 주소로 접속한 유저를 확인
	// 없을 시 세샌 생성
	if (!SessionManager::getInstance().FindToAdd(inAdd))
	{
		Session * client = new IocpSession(inClinentSock, inAdd);
		SessionManager::getInstance().Insert(client);
		
		CreateIoCompletionPort((handle_t)client->GetSocket(), mIocpHandle, (ULONG_PTR)&client, 0);
		
		client->RecvPacket();
	}
	
}