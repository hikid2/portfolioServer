#include "stdafx.h"
#include "ClientProxy.h"


ClientProxy::ClientProxy()
{
	mSession = nullptr;
	mController = nullptr;
}


ClientProxy::~ClientProxy()
{
}

void ClientProxy::Clear()
{
	mSession = nullptr;
	mController = nullptr;
}



/**
@return : void

@param 1 : Session Pointer

@brief : 세션주소를 이용하여 ClientProxy의 Session의 Detach를 진행

@warning :
*/
void ClientProxyManager::Erase(Session * inSession)
{
	auto FindResult = this->Find(inSession);
	if (FindResult != nullptr) {
		FindResult->SetUse(false);
		FindResult->DetachSession();
		//FindResult->RemoveController();
	}
}

/**
@return : CientProxy Pointer (Not Found 시 Nullptr 반환)

@param 1 : Session ptr

@brief : 세션 포인터를 통해 ClientProxy배열에 있는 ClinetProxy를 Find

@warning : 람다사용. 에러 생길시 이부분일 확률이 높음
*/
ClientProxy * ClientProxyManager::Find(Session * inSession)
{
	try
	{
		auto inPtr = std::find_if(mProxyAry.begin(), mProxyAry.end(), [&inSession](array<ClientProxy *, MAXPLAYER>::value_type ptr)
		{
			if (ptr->IsUse() && (inSession == ptr->GetSession()))
			{
				return ptr;
			}
		});
		if (inPtr == mProxyAry.end())
			throw exception("Not Found ProxyAry..");

		return *inPtr;
	}
	catch (const std::exception& ex)
	{
		Log("%s", ex.what());
		return nullptr;
	}
}
