#pragma once
#include"stdafx.h"
// static local singleton
template<class T>
class Singleton
{
protected:
	Singleton() {}
	~Singleton() {}
	Singleton(const Singleton&);
public:
	Singleton& operator = (const Singleton &);
	static T& getInstance()
	{
		static T instance;
		return instance;
	}
};