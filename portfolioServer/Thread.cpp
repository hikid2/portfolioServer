#include "stdafx.h"
#include "Thread.h"


Thread::Thread(thread * inThread, wstring inName)
{
	static int32_t id = 0;
	mId = id++;
	mThread = inThread;
	mName = inName;
	mLock = new Lock(inName);
}

Thread::~Thread()
{
	mThread->join();
	SAFEDELETE(mThread);
	SAFEDELETE(mLock);
}
