#pragma once
#include<array>
#include<cstdarg>

#pragma warning(disable:4996)
// Time Headers...

#define Log(Str, ...) CLog::getInstance().PrintLog(Str, __VA_ARGS__);


#include<chrono>
#include<ctime>
#include"Singleton.h"

using namespace std;
using namespace std::chrono;


class CLog : public Singleton<CLog>
{
public:

	typedef enum
	{
		Print,
		FileWrite,
		Both,
	}LOGTYPE;

	
public:
	CLog()
	{

	}
	virtual ~CLog()
	{

	}
	void PrintLog(char * ch, ...)
	{
		array<char, 400> PrintBuf;
		system_clock::time_point p = system_clock::now();
		time_t nowTime = system_clock::to_time_t(p);
		auto * tm = localtime(&nowTime);
		tm->tm_year;
		sprintf(PrintBuf.data(), "[ CLOCK : %d-%d-%d %d:%d:%d \t Log Message : ", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
		va_list ap;

		va_start(ap, ch);
		vsprintf(PrintBuf.data() + strlen(PrintBuf.data()), ch, ap);
		va_end(ap);
		strcpy(PrintBuf.data() + strlen(PrintBuf.data()),"\t]");
		printf("%s\n", PrintBuf.data());
	}
	void PrintLog(const wchar_t * ch, ...)
	{
		array<wchar_t, 400> PrintBuf;
		system_clock::time_point p = system_clock::now();
		time_t nowTime = system_clock::to_time_t(p);
		auto * tm = localtime(&nowTime);
		tm->tm_year;
		swprintf(PrintBuf.data(), L"[ CLOCK : %d-%d-%d %d:%d:%d \t Log Message : ", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
		va_list ap;
		va_start(ap, ch);
		vswprintf(PrintBuf.data() + lstrlenW(PrintBuf.data()), ch, ap);
		//vsprintf(PrintBuf.data() + strlen(PrintBuf.data()), ch, ap);
		va_end(ap);
		lstrcpyW(PrintBuf.data() + lstrlenW(PrintBuf.data()), L"\t]");
		wprintf(L"%s\n", PrintBuf.data());
	}
private:
	//LOGTYPE mLogtype;
	
};

